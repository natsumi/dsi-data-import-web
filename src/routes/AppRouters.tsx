import { useEffect } from "react";
import { useNavigate,useRoutes } from "react-router-dom"
import Root from "../pages/Root"
import Login from "../pages/Login"
import Register from "../pages/Register"
import ForgotPassword from "../pages/ForgotPassword"
import CaseManagement from "../pages/CaseManagement"
import CreateCase from "../pages/CaseManagement/CreateCase"
import Permission from "../pages/Permission"
import Dashboard from "../pages/Dashboard"
import PermissionList from "../pages/Permission/PermissionManagement /PermissionList";
import UserManagementList from "../pages/Permission/UserManagement /UserManagementList";

const AppRouters = () => {
    const element = useRoutes([
        {
            path: '/',
            element: <Root />,
            children: [
                {
                    path: '',
                    element: <Dashboard />
                },
                {
                    path:'permission',
                    element:<Permission/>,
                },
                {
                    path: 'permission/role-management',
                    element: <PermissionList />
                },

                {
                    path: 'permission/user-management',
                    element: <UserManagementList />
                },
                {
                    path:'case-management',
                    element:<CaseManagement/>
                },
                {
                    path: 'create',
                    element: <CreateCase />
                },
            ]
        },
        {
            path: 'login',
            element: <Login />
        },
        {
            path: 'register',
            element: <Register />
        },
        {
            path: 'forgot-password',
            element: <ForgotPassword />
        },
    ])
    const token = localStorage.getItem('accessToken');
    const navigate = useNavigate();

    useEffect(() => {
        const publicPaths = ['/login', '/register', '/forgot-password'];
        const currentPath = window.location.pathname;
    
        if (!token && !publicPaths.includes(currentPath)) {
          navigate("/login", { replace: true });
        }
      }, [navigate, token]);

    return element
}

export default AppRouters