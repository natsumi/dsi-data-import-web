import { z } from "zod";

const loginSchema = z.object({
    email: z.string().min(1, 'กรุณากรอกอีเมล').email('รูปแบบอีเมลไม่ถูกต้อง'),
    password: z.string().min(1, 'กรุณากรอกรหัสผ่าน'),
});

const registrationSchema = z.object({
    firstName: z.string().min(1, 'กรุณากรอกชื่อ'),
    lastName: z.string().min(1, 'กรุณากรอกนามสกุล'),
    email: z.string().min(1, 'กรุณากรอกอีเมล').email('รูปแบบอีเมลไม่ถูกต้อง'),
    phone: z.string().regex(/^\d{10}$/, "กรุณากรอกเบอร์โทรศัพท์"),
    position: z.string().min(1, 'กรุณากรอกตำแหน่ง'),
    passwordRegis: z.string().min(8, "กรุณากรอกรหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร"),
    confirmPassword: z.string().min(8, "ยืนยันรหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร"),
    }).refine((data) => data.passwordRegis === data.confirmPassword, {
        message: "ยืนยันรหัสผ่านไม่ถูกต้อง",
        path: ["confirmPassword"], // path of error
});

export {
    registrationSchema,
    loginSchema,
  };