import axiosInstant from '../../config/axiosInstant';

export const login = async (username: string, password: string) => {
  try {
    const response = await axiosInstant.post(
    '/posts', 
    {
      username,
      password
    }
    );
    localStorage.setItem('token', 'test-local-storage');
    return response.data;
  } catch (error) {
    console.error("Error during login:", error);
    throw error;
  }
};
