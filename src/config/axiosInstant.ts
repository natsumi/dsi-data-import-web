import axios, { AxiosError } from "axios";

const TOKEN = localStorage.getItem('accessToken');

const axiosInstant = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
});

axiosInstant.interceptors.request.use(
  (config) => {
    config.headers.Authorization = TOKEN;
    return config;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);

export default axiosInstant;