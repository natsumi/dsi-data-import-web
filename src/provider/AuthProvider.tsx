import { AxiosError } from 'axios';
import { ReactNode, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstant from '../config/axiosInstant';

function AuthProvider({
    children,
}: { children: ReactNode}
) {
    const navigate = useNavigate();

    const refreshToken = async () => {
        try {
          const response = await axiosInstant.post('/refresh-token', {
            refreshToken: localStorage.getItem('refreshToken')
          });
          const newToken = response.data.token;
          localStorage.setItem('token', newToken);
          return true;
        } catch (error) {
          console.error('Failed to refresh token:', error);
          return false;
        }
      };

    useEffect(() => {
        const responseInterceptor = axiosInstant.interceptors.response.use(
            function (response) {
              return response;
            },
            async function (error: unknown | AxiosError) {
              if (error instanceof AxiosError) {
                if (error.response?.status === 401) {
                  console.log(error);
                  const getRefreshToken = await refreshToken();
                  if (getRefreshToken) {
                    console.log("Refresh New");
                    return axiosInstant(error.config?.url ?? "");
                  }else{
                      localStorage.clear();
                      navigate("/login");
                  }
                  console.log({ getRefreshToken });
                }
              }
              return Promise.reject(error);
            }
          );

    return () => axiosInstant.interceptors.response.eject(responseInterceptor);
    },[])

    
  return (
    <>{children}</>
  )
}

export default AuthProvider