import {  useState } from "react";
import { useNavigate } from "react-router-dom"
import {  MenuItem, MenuList, Paper, Typography } from "@mui/material";

import SettingsIcon from "@mui/icons-material/Settings";
import AutoAwesomeMosaicIcon from "@mui/icons-material/AutoAwesomeMosaic";
import ArticleIcon from "@mui/icons-material/Article";
import HubIcon from '@mui/icons-material/Hub';


const Sidebar = () => {
  const navigate = useNavigate()

  return (
    <Paper sx={{color:'white', backgroundColor:'#273c75'}}>
      
      <MenuList >
        <MenuItem onClick={() => {
            navigate('/case-management ')
        }}>
          <ArticleIcon />
          <Typography variant="inherit">การจัดการเคส</Typography>
        </MenuItem>
        <MenuItem onClick={() => {
            navigate('')
        }}>
          <AutoAwesomeMosaicIcon />
          <Typography variant="inherit">แผนภาพข้อมูล</Typography>
        </MenuItem>
        <MenuItem onClick={() => {
            navigate('')
        }}>
        <HubIcon/>
          <Typography variant="inherit">แผนฝังเครือข่าย</Typography>
        </MenuItem>
        <MenuItem onClick={() => {
            navigate('permission')
        }}>
          <SettingsIcon />
          <Typography variant="inherit">การจัดการสิทธิ์</Typography>
        </MenuItem>
      </MenuList>
    </Paper>
  );
};

export default Sidebar;

{
  /* 
  Nevigate to another page with link.
  ref: https://reactrouter.com/en/main/components/link#link 

  Nevigate to another page with nevigate hook function.
  ref: https://reactrouter.com/en/main/hooks/use-navigate
*/
}
