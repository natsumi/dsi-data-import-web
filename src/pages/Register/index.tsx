import React from 'react';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { Box, Button, Container, TextField, Typography, Snackbar, Alert } from '@mui/material';
import { Link, useNavigate } from "react-router-dom";
import { z } from 'zod';
import { registrationSchema } from "../../utils/validate";

type RegisterFormInputs = z.infer<typeof registrationSchema>;

const Register = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterFormInputs>({
    resolver: zodResolver(registrationSchema),
  });

  const [open, setOpen] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');
  const [successMessage, setSuccessMessage] = React.useState('');

  const navigate = useNavigate();

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  const onSubmit = async (data: RegisterFormInputs) => {
    try {
      // Perform registration logic here
      console.log('Registration data:', data);
      setSuccessMessage('ลงทะเบียนสำเร็จ');
      setOpen(true);
      await new Promise(resolve => setTimeout(resolve, 2000));
      navigate("/login");
    } catch (error) {
      setErrorMessage('ลงทะเบียนไม่สำเร็จ, โปรดลองอีกครั้ง');
      console.error('Registration failed:', error);
      setOpen(true);
    }
  };

  return (
    <Container maxWidth="sm">
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        height="100vh"
        sx={{ padding: 2 }}
      >
        <Typography variant="h4" component="h1" gutterBottom>
          ลงทะเบียน
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box mb={2}>
            <TextField
              fullWidth
              label="ชื่อ"
              variant="outlined"
              {...register('firstName')}
              error={!!errors.firstName}
              helperText={errors.firstName ? errors.firstName.message : ''}
            />
          </Box>
          <Box mb={2}>
            <TextField
              fullWidth
              label="นามสกุล"
              variant="outlined"
              {...register('lastName')}
              error={!!errors.lastName}
              helperText={errors.lastName ? errors.lastName.message : ''}
            />
          </Box>
          <Box mb={2}>
            <TextField
              fullWidth
              label="เบอร์โทรศัพท์"
              variant="outlined"
              type="text"
              {...register('phone')}
              error={!!errors.phone}
              helperText={errors.phone ? errors.phone.message : ''}
              inputProps={{ maxLength: 10 }}
              onInput={(e) => {
                const input = e.target as HTMLInputElement;
                input.value = input.value.replace(/[^0-9]/g, '');
                if (input.value.length > 10) {
                  input.value = input.value.slice(0, 10);
                }
              }}
            />
          </Box>
          <Box mb={2}>
            <TextField
              fullWidth
              label="อีเมล"
              variant="outlined"
              {...register('email')}
              error={!!errors.email}
              helperText={errors.email ? errors.email.message : ''}
            />
          </Box>
          <Box mb={2}>
            <TextField
              fullWidth
              label="รหัสผ่าน"
              type="password"
              variant="outlined"
              {...register('passwordRegis')}
              error={!!errors.passwordRegis}
              helperText={errors.passwordRegis ? errors.passwordRegis.message : ''}
            />
          </Box>
          <Box mb={2}>
            <TextField
              fullWidth
              label="ยืนยันรหัสผ่าน"
              type="password"
              variant="outlined"
              {...register('confirmPassword')}
              error={!!errors.confirmPassword}
              helperText={errors.confirmPassword ? errors.confirmPassword.message : ''}
            />
          </Box>
          <Box mb={2}>
            <TextField
              fullWidth
              label="ตำแหน่ง"
              variant="outlined"
              {...register('position')}
              error={!!errors.position}
              helperText={errors.position ? errors.position.message : ''}
            />
          </Box>
          <Button variant="contained" color="primary" type="submit" fullWidth>
            ลงทะเบียน
          </Button>

          <Box p={1}><hr/></Box>

          <Link to="/login">
            <Button variant="contained" color="inherit" fullWidth>
                ย้อนกลับ
            </Button>
          </Link>
        </form>
        <Snackbar
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
        >
          <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
            {errorMessage}
          </Alert>
        </Snackbar>
        <Snackbar
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
        >
          <Alert onClose={handleClose} severity="success" sx={{ width: "100%" }}>
            {successMessage}
          </Alert>
        </Snackbar>
      </Box>
    </Container>
  );
};

export default Register;