import { zodResolver } from "@hookform/resolvers/zod";
import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
  Snackbar,
  Alert,
  IconButton,
  InputAdornment,
} from "@mui/material";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import { z } from "zod";
import { loginSchema } from "../../utils/validate";
import {
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
} from "firebase/auth";
import { auth } from "../../config/firebase";
import { useState } from "react";
import Cookies from "js-cookie";
import { Visibility, VisibilityOff } from "@mui/icons-material";

type LoginFormInputs = z.infer<typeof loginSchema>;

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormInputs>({
    resolver: zodResolver(loginSchema),
  });

  const [open, setOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const handleClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const navigate = useNavigate();

  // ********* Wait comfirm *********
  // const signInWithGoogle = async () => {
  //   try {
  //     const provider = new GoogleAuthProvider();

  //     const result = await signInWithPopup(auth, provider);

  //     const credential = GoogleAuthProvider.credentialFromResult(result);
  //     if (!credential) {
  //       console.error("Error in user Credential");
  //       return;
  //     }
  //     const token = credential.accessToken;
  //     const user = result.user;
  //     console.log(user, token);
  //   } catch (error) {
  //     setErrorMessage("Google Sign-In failed. Please try again.");
  //     setOpen(true);
  //     console.error("Google Sign-In failed:", error);
  //   }
  // };

  const onSubmit = async (data: LoginFormInputs) => {
    try {
      const credential = await signInWithEmailAndPassword(
        auth,
        data.email,
        data.password
      );
      const accessToken = (await credential.user.getIdTokenResult()).token;
      localStorage.setItem("accessToken", accessToken);
      Cookies.set("refreshToken", credential.user.refreshToken, { expires: 1 });
      navigate("/");
    } catch (error) {
      setErrorMessage("อีเมล หรือ รหัสผ่าน ไม่ถูกต้อง!");
      setOpen(true);
      console.error("Login failed:", error);
    }
  };

  return (
    <Container maxWidth="sm">
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        height="100vh"
        sx={{ padding: 2 }}
      >
        <Typography variant="h4" component="h1" gutterBottom>
          เข้าสู่ระบบ
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box mb={2}>
            <TextField
              fullWidth
              label="อีเมล"
              variant="outlined"
              {...register("email")}
              error={!!errors.email}
              helperText={errors.email ? errors.email.message : ""}
            />
          </Box>
          <Box>
            <TextField
              fullWidth
              label="รหัสผ่าน"
              type={showPassword ? "text" : "password"}
              variant="outlined"
              {...register("password")}
              error={!!errors.password}
              helperText={errors.password ? errors.password.message : ""}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </Box>

          <Box p={1} textAlign={"end"}>
            <Link to="/forgot-password">ลืมรหัสผ่าน ?</Link>
          </Box>

          <Button variant="contained" color="primary" type="submit" fullWidth>
            เข้าสู่ระบบ
          </Button>

          <Box p={1}>
            <hr />
          </Box>

          <Link to="/register">
            <Button variant="contained" color="inherit" fullWidth>
              ลงทะเบียน
            </Button>
          </Link>
        </form>

        {/* ********* Wait comfirm ********* */}
        {/* <Button variant="contained" color="primary" fullWidth onClick={signInWithGoogle}>
            google
        </Button> */}

        <Snackbar
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
        >
          <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
            {errorMessage}
          </Alert>
        </Snackbar>
      </Box>
    </Container>
  );
};

export default Login;
