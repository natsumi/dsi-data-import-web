import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import { Box, Button, Grid } from "@mui/material";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import { height } from "@fortawesome/free-solid-svg-icons/fa0";

const test = {
  name: "myname  natsumi",
  phone: "0974653373",
  email: "ghlbu@gmail.com",
};
// ,{
//   'name':'myname  not',
//   'phone': '0974620293',
//   'email':'gjk;uo@gmail.com'
// }

const PermissionList = () => {
  return (
    <Grid container height="100%" columnSpacing={2} padding={2}>
      <Grid item xs={6}>
        <Paper sx={{ height: "100%" }} elevation={6}>
        <List>
            <ListItem>{test.name}</ListItem>
          </List>
        </Paper>
      </Grid>
      <Grid item xs={6} height="100%">
        <Paper
          sx={{
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Button sx={{ textAlign: "center" }} variant="outlined" size="large">
            {" "}
            create RolePermission
          </Button>
        </Paper>
      </Grid>
      <Paper />
    </Grid>
  );
};
export default PermissionList;
