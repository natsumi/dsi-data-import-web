import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import EditNoteIcon from "@mui/icons-material/EditNote";
import { indigo } from "@mui/material/colors";
import SearchIcon from "@mui/icons-material/Search";
import {
  Avatar,
  Box,
  Button,
  Chip,
  FormControlLabel,
  InputAdornment,
  Stack,
  Switch,
  TablePagination,
} from "@mui/material";
import { useState } from "react";
import { width } from "@fortawesome/free-solid-svg-icons/fa0";

const StyledTableCell = styled(TableCell)(() => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#273c75",
    color: "white",
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

// const StyledTableRow = styled(TableRow)(() => ({
//   "&:nth-of-type(odd)": {
//     backgroundColor: "white",
//   },
//   // hide last border
//   "&:last-child td, &:last-child th": {
//     // border: 0,
//   },
// }));

function createData(name: string, role: string[], status: boolean) {
  return { name, role, status };
}

const rows = [
  createData("Pisitsin Kumsuk", ["user", "guest"], false),
  createData("Kanrada Phankong", ["user", "guest", "dev"], true),
  createData("Ketsiree Tantiwit", ["user"], true),
  createData("Papatsorn Ngamrussameewong", ["user"], true),
  createData("Wirapat Prigphet", ["admin"], true),
  createData("Kanrada Phankong", ["user", "guest", "dev"], true),
  createData("Ketsiree Tantiwit", ["user"], true),
  createData("Papatsorn Ngamrussameewong", ["user"], true),
  createData("Wirapat Prigphet", ["admin"], true),
  createData("Kanrada Phankong", ["user", "guest", "dev"], true),
  createData("Ketsiree Tantiwit", ["user"], true),
  createData("Papatsorn Ngamrussameewong", ["user"], true),
  createData("Wirapat Prigphet", ["admin"], true),
  createData("Kanrada Phankong", ["user", "guest", "dev"], true),
  createData("Ketsiree Tantiwit", ["user"], true),
  createData("Papatsorn Ngamrussameewong", ["user"], true),
  createData("Wirapat Prigphet", ["admin"], true),
];

const UserManagementList = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [status, setStatus] = useState(true);
  function handleClick() {
    setStatus(true);
  }
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Stack height="100%" spacing={2} padding={2}>
      <Stack direction="row" justifyContent="space-between">
        <TextField
          id="outlined-search"
          label="Search field"
          type="search"
          size="small"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />

        <Button variant="outlined" size="small" endIcon={<EditNoteIcon />}>
          Edit
        </Button>
      </Stack>

      <Paper>
        <TableContainer
          sx={{ maxHeight: 600 }}
          //   component={Paper}
          //   sx={{ boxShadow: (theme) => theme.shadows[3] }}
        >
          <Table stickyHeader aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell sx={{ width: "5%" }}></StyledTableCell>
                <StyledTableCell sx={{ width: "50%" }}>Name</StyledTableCell>
                <StyledTableCell sx={{ width: "30%" }}>
                  User Role
                </StyledTableCell>
                <StyledTableCell sx={{ width: "20%" }}>Status</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, i) => (
                  <TableRow key={i}>
                    <StyledTableCell>
                      <Avatar />
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      <Box display="flex">{row.name}</Box>
                    </StyledTableCell>
                    <StyledTableCell>
                      <Stack direction="row" spacing={1}>
                        {row.role.map((item) => (
                          <Chip
                            label={item}
                            color="primary"
                            variant="outlined"
                          />
                        ))}
                      </Stack>
                    </StyledTableCell>
                    <StyledTableCell>
                      <FormControlLabel
                        sx={{
                          display: "block",
                        }}
                        control={
                          <Switch
                            checked={row.status}
                            onChange={() => setStatus(!status)}
                            name="status"
                            color="primary"
                          />
                        }
                        label={row.status ? "active" : "inactive"}
                      />
                    </StyledTableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Stack>
  );
};
export default UserManagementList;
