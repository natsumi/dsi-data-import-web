import { Button, Grid } from "@mui/material"
import PermissionList from "./PermissionManagement /PermissionList"
import PermissionDetail from "./PermissionManagement /PermissionDetail"
import { useNavigate } from "react-router-dom"


const Permission = () => {
    const navigate = useNavigate()

    return (
        // <Grid>
        <Grid spacing={4}>
        <Button onClick={() => {
            navigate('/permission/role-management ')
        }}>Go to Permission List</Button>

        <Button onClick={() => {
            navigate('/permission/user-management ')
        }}>Go to User List</Button>
        </Grid>
        // <PermissionList />
        //</Grid>
        // <Grid>
        // <PermissionDetail/>
        // </Grid>
    )
}

export default Permission