import * as React from 'react';
import { Link } from 'react-router-dom';
import FormControl from '@mui/material/FormControl';
import { Stack,Box,IconButton,TextField,InputLabel,MenuItem,Divider } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import Select, { SelectChangeEvent } from '@mui/material/Select';

const CreateCase = () => {
    const [age, setAge] = React.useState('');

    const handleChange = (event: SelectChangeEvent) => {
      setAge(event.target.value);
    };

    return (
        <>
            {/* back page */}
            <Box
                p={2}
                textAlign={'start'}
            >
                <Link to="/">
                    <IconButton aria-label="delete">
                        <ArrowBackIosIcon />
                    </IconButton>
                </Link>
                Create Case
            </Box>

            {/* data create */}
            <Stack
                direction="row"
                divider={<Divider orientation="vertical" />}
                spacing={10}
                justifyContent="center"
            >
                <Stack
                    direction="column"
                    spacing={2}
                    justifyContent="center"
                >
                    <Box
                        py={2}
                        px={5}
                        textAlign={'start'}
                    >
                        <TextField sx={{ minWidth: 300}}
                            id="caseName"
                            label="Name"
                        />
                    </Box>
                    <Box
                        pb={2}
                        px={4}
                        textAlign={'start'}
                    >
                        <FormControl sx={{ m: 1, minWidth: 300 }}>
                            <InputLabel id="demo-simple-select-helper-label">Type</InputLabel>
                            <Select
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={age}
                            label="Age"
                            onChange={handleChange}
                            >
                            <MenuItem value="">
                                <em>-</em>
                            </MenuItem>
                            <MenuItem value={10}>สืบสวน</MenuItem>
                            <MenuItem value={20}>สอบสวน</MenuItem>
                            <MenuItem value={30}>การข่าว</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                    <Box
                        px={5}
                        textAlign={'start'}
                    >
                        <TextField
                            sx={{ minWidth: 300}}
                            id="desCase"
                            label="Description"
                            multiline
                            minRows={4}
                            maxRows={4}
                        />
                    </Box>
                </Stack>
                <Stack
                    direction="column"
                    spacing={2}
                    justifyContent="start"
                >
                    <Box
                        pt={1}
                        px={4}
                        textAlign={'start'}
                    >
                        <FormControl sx={{ m: 1, minWidth: 300 }}>
                            <InputLabel id="demo-simple-select-helper-label">Type</InputLabel>
                            <Select
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={age}
                            label="Age"
                            onChange={handleChange}
                            >
                            <MenuItem value="">
                                <em>-</em>
                            </MenuItem>
                            <MenuItem value={10}>สืบสวน</MenuItem>
                            <MenuItem value={20}>สอบสวน</MenuItem>
                            <MenuItem value={30}>การข่าว</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                </Stack>
            </Stack>
        </>
    )
}

export default CreateCase