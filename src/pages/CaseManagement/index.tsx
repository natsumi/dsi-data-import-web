import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const CaseManagement = () => {
    return (
        <>
            <Box
                p={2}
                textAlign={'end'}
                >
                <Link to="/create">
                    <Button variant="contained" color="primary">
                        Create
                    </Button>
                </Link>
            </Box>
        </>
    )
}

export default CaseManagement