import { Outlet } from 'react-router-dom';
import Sidebar from '../../components/sidebar/sidebar';
import './index.css';
import Navbar from '../../components/navbar/navbar';
import { Box, Grid } from '@mui/material';
import AuthProvider from '../../provider/AuthProvider';

function Root() {
    return (
        <AuthProvider>
            <Grid container flexDirection='column'>
                <Grid item flex={1} >
                <Navbar />
                </Grid>
                <Grid item  flex='100vh' display='flex' flexDirection='row' >
                <Sidebar/>
                    <Box flex={1} overflow='auto' bgcolor='ghostwhite'>
                        <Outlet />
                    </Box>
                </Grid>
            </Grid>
        </AuthProvider>
    )
}

export default Root
